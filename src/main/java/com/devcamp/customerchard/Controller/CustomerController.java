package com.devcamp.customerchard.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerchard.Repository.CustomerRepository;

@RestController
@CrossOrigin
public class CustomerController {
    @Autowired
    CustomerRepository iCustomerRepository;

    @GetMapping("/customers")
    public ResponseEntity<Object> getAllCustomers() {
        try {
            return new ResponseEntity<>(iCustomerRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customers/like")
    public ResponseEntity<Object> getAllCustomerLike() {
        try {
            return new ResponseEntity<>(iCustomerRepository.findCustomerByCountryLike(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
