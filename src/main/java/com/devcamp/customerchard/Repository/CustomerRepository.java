package com.devcamp.customerchard.Repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.customerchard.Entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer>{
    @Query(value = "SELECT * FROM customers WHERE country IN ('USA', 'France', 'Singapore', 'Spain')", nativeQuery = true)
    List<Customer> findCustomerByCountryLike();
}
