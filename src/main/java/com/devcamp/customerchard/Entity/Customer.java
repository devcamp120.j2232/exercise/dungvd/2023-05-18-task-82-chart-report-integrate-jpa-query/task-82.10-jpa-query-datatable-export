package com.devcamp.customerchard.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Input last name")
    @JoinColumn(name = "last_name")
    private String lastName;

    @NotEmpty(message = "Input first name")
    @JoinColumn(name = "first_name")
    private String firstName;

    @NotEmpty(message = "Input phone number")
    @JoinColumn(name = "phone_number")
    private String phoneNumber;

    private String address;

    private String city;

    private String state;

    @JoinColumn(name = "postal_code")
    private String postalCode;

    private String country;

    @JoinColumn(name = "sales_rep_employee_number")
    private int saleRepEmployeeNumber;

    @JoinColumn(name = "credit_limit")
    private int creditLimit;

    public Customer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getSaleRepEmployeeNumber() {
        return saleRepEmployeeNumber;
    }

    public void setSaleRepEmployeeNumber(int saleRepEmployeeNumber) {
        this.saleRepEmployeeNumber = saleRepEmployeeNumber;
    }

    public int getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(int creditLimit) {
        this.creditLimit = creditLimit;
    }

    
}
