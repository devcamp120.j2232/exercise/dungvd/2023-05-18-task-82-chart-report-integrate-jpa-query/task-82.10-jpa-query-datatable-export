package com.devcamp.customerchard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerChardApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerChardApplication.class, args);
	}

}
